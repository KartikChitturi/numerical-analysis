#include <stdio.h>
#include <stdlib.h>


void run_gauss_seidel_method( int n, double A[3][3], double b[3], double epsilon, int *numit, double *x );


int main(int argc, char *argv[]){
  int n,i;
  n = 3;
  double A[3][3] = {{5,-2,3},{-3,9,1},{2,-1,-7}};
  double b[3];

  b[0] = -1 ;
  b[1] = 2 ;
  b[2] = 3 ;

  double x[3];
  for(i=0; i<n; i++){
     x[i] = 0.0;
  }
  double eps = 1.0e-4;
  int cnt = 0;
  run_gauss_seidel_method(n,A,b,eps,&cnt,x);

  double sum = 0.0;
  for(i=0; i<n; i++){
    double d = x[i] - 1.0;
    sum += (d >= 0.0) ? d : -d;
  }
    
  return 0;
}

void run_gauss_seidel_method( int n, double A[3][3], double b[3], double epsilon, int *numit, double *x ){
  
  double *dx = (double*) calloc(n,sizeof(double));
   
  int i,j,k;

  double sum = 1.0;
  while(sum>epsilon){
    sum = 0.0;
    for(i=0; i<n; i++){
        dx[i] = b[i];
        for(j=0; j<n; j++){
          dx[i] -= A[i][j]*x[j];
        }
        dx[i] /= A[i][i]; x[i] += dx[i];
        sum += ( (dx[i] >= 0.0) ? dx[i] : -dx[i]);
      }
  }
  *numit = k+1; free(dx);
  printf("Using the Gauss-Seidel method, the values of x are:\n");
  for(i=0;i<3;i++){
    printf("%10.6f\n",x[i]);
  }
}
