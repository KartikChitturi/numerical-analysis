#include<stdio.h>
float Dinv[10][10];
float approx[10][1];
float R[10][10];
float matrixRes[10][1];
float b[3][1];
float temp[10][1];
int row,column,size,navigate;



void multiply(float matrixA[][10],float matrixB[][1]){
  int ctr,ictr;
  for(ctr=0;ctr<size;ctr++){
    matrixRes[ctr][0]=0;
    for(navigate=0;navigate<size;navigate++){
            matrixRes[ctr][0]=matrixRes[ctr][0]+matrixA[ctr][navigate]*matrixB[navigate][0];
    }
  }
}


int main(int argc, char *argv[]){
  size = 3;
  float coeff[3][3] = {{5,-2,3},{-3,9,1},{2,-1,-7}};

  for(row=0;row<size;row++){
    approx[row][0] = 0;
  }

  b[0][0] = -1 ;
  b[1][0] = 2 ;
  b[2][0] = 3 ;

  for(row=0;row<size;row++){
    for(column=0;column<size;column++){
        if(row==column)
          Dinv[row][column]=1/coeff[row][column];
        else
          Dinv[row][column]=0;
     }
  }

  for(row=0;row<size;row++){
    for(column=0;column<size;column++){
      if(row==column)
        R[row][column]=0;
      else
        if(row!=column)
          R[row][column]=coeff[row][column];
    }
  }


  int octr;
  float prev[10][1];
   

    multiply(R,approx);
    for(row=0;row<size;row++){
      temp[row][0]=b[row][0]-matrixRes[row][0];
    }
    multiply(Dinv,temp);
    for(octr=0;octr<size;octr++){
      approx[octr][0]=matrixRes[octr][0];
    }

  for(row=0;row<size;row++){
    prev[row][0] = approx[row][0];
  }

  float maxDiff=100000;

  while(maxDiff>0.0001){
    maxDiff = 0;
    multiply(R,approx);
    for(row=0;row<size;row++){
      temp[row][0]=b[row][0]-matrixRes[row][0];
    }
    multiply(Dinv,temp);
    for(octr=0;octr<size;octr++){
      approx[octr][0]=matrixRes[octr][0];
    }
    
    for(row=0;row<size;row++){
      if(fabsf(approx[row][0]-prev[row][0])>maxDiff)
        maxDiff = fabsf(approx[row][0]-prev[row][0]);
    }

    for(row=0;row<size;row++){
      prev[row][0] = approx[row][0];
    }


      }
    printf("Using the Jacobi method the values of x are:\n");
    for(row=0;row<size;row++){
      printf("%10.6f\n",approx[row][0]);
    }


}


