#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include "parameters.h"

main(){
  double x_start=0.0;
  double x_end=1.0;

  int Ns[]={11,101,1001,10001,100001,1000001,10000001};

  int i=0;
  int j=0;
  int m;
  double xi;
  double h;
  double sumMid,sumTrap,sumSimp;
  double intMid,intTrap,intSimp;

  printf("    N          Midpt            Trapz            Simps           err_Mdpt        err_Trapz       err_Simps\n");
  for(i=0;i<7;i++){
    m=Ns[i]-1;
    h=(x_end-x_start)/m;
    sumMid=0;
    sumTrap=0;
    sumSimp=0;
    for(j=0;j<m;j++){
      xi=x_start+(j*h);
      sumMid+=midp(xi,h);
      sumTrap+=trap(xi,h);
      if(j%2==1){
        sumSimp+=simp(xi,h);
      }
    }
    intMid=sumMid*h;
    intTrap=sumTrap*(h/2);
    intSimp=sumSimp*(h/3);
    printf("%8d  %.13f  %.13f  %.13f  %.13f %.13f %.13f\n",m+1,intMid,intTrap,intSimp,fabs(.125-intMid),fabs(.125-intTrap),fabs(.125-intSimp));
  }
return 0;
}
