#include<stdio.h>
#include<stdlib.h>
#include<math.h>


void print_Matrix(double array[64]);
void print_Vector(double array[8]);

int main(){

  int n=8,nrhs=8,lda=8,ldb=8,info;
  int ipiv[8];

  double B[64] = {
    1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
    1.0,(1.0/7.0),pow((1.0/7.0),2.0),pow((1.0/7.0),3.0),pow((1.0/7.0),4.0),pow((1.0/7.0),5.0),pow((1.0/7.0),6.0),pow((1.0/7.0),7.0),
    1.0,(2.0/7.0),pow((2.0/7.0),2.0),pow((2.0/7.0),3.0),pow((2.0/7.0),4.0),pow((2.0/7.0),5.0),pow((2.0/7.0),6.0),pow((2.0/7.0),7.0),
    1.0,(3.0/7.0),pow((3.0/7.0),2.0),pow((3.0/7.0),3.0),pow((3.0/7.0),4.0),pow((3.0/7.0),5.0),pow((3.0/7.0),6.0),pow((3.0/7.0),7.0),
    1.0,(4.0/7.0),pow((4.0/7.0),2.0),pow((4.0/7.0),3.0),pow((4.0/7.0),4.0),pow((4.0/7.0),5.0),pow((4.0/7.0),6.0),pow((4.0/7.0),7.0),
    1.0,(5.0/7.0),pow((5.0/7.0),2.0),pow((5.0/7.0),3.0),pow((5.0/7.0),4.0),pow((5.0/7.0),5.0),pow((5.0/7.0),6.0),pow((5.0/7.0),7.0),
    1.0,(6.0/7.0),pow((6.0/7.0),2.0),pow((6.0/7.0),3.0),pow((6.0/7.0),4.0),pow((6.0/7.0),5.0),pow((6.0/7.0),6.0),pow((6.0/7.0),7.0),
    1.0,1.0,1.0,1.0,1.0,1.0,1.0,1.0
    };
    //printf("Matrix B\n");
    //print_Matrix(B);

  double B_I[64] = {
    1.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,
    0.0,1.0,0.0,0.0,0.0,0.0,0.0,0.0,
    0.0,0.0,1.0,0.0,0.0,0.0,0.0,0.0,
    0.0,0.0,0.0,1.0,0.0,0.0,0.0,0.0,
    0.0,0.0,0.0,0.0,1.0,0.0,0.0,0.0,
    0.0,0.0,0.0,0.0,0.0,1.0,0.0,0.0,
    0.0,0.0,0.0,0.0,0.0,0.0,1.0,0.0,
    0.0,0.0,0.0,0.0,0.0,0.0,0.0,1.0
    };
    
  dgesv(&n,&nrhs,B,&lda,ipiv,B_I,&ldb,&info);
  //printf("Matrix B^-1\n");
  //print_Matrix(B_I);


  double weight_vec[8] = {
    1.0,
    0.5,
    pow(0.5,2.0),
    pow(0.5,3.0),
    pow(0.5,4.0),
    pow(0.5,5.0),
    pow(0.5,6.0),
    pow(0.5,7.0)
  };
  
   
  double C[8];
  int one = 1;
  double alpha = 1.0;
  double beta = 0.0;
  char *lN = "N";
  char *lT = "T";
  
  dgemv(lN,&n,&n,&alpha,B_I,&n,weight_vec,&one,&beta,C,&one);
  printf("\n---------------------------------------------------------------\n");
  printf("Interpolation\n");
  printf("Weights\n");
  print_Vector(C);

  int i = 0;
  double sol = 0;
  for(i=0;i<8;i++){
    sol += C[i]*pow((((double)i)/7.0),7.0);
  }
  printf("x=0.5   numerical value   analytical value    absolute error\n");  
  printf("0.5     %13.10f      %13.10f      %13.10f\n",sol,pow(0.5,7.0),fabs(sol - pow(0.5,7.0)));


  double weight_first_dif[8] = { 
    0.0,
    1.0,
    2.0*0.5,
    3.0*pow(0.5,2.0),
    4.0*pow(0.5,3.0),
    5.0*pow(0.5,4.0),
    6.0*pow(0.5,5.0),
    7.0*pow(0.5,6.0)
  };


  dgemv(lN,&n,&n,&alpha,B_I,&n,weight_first_dif,&one,&beta,C,&one);
  printf("\n--------------------------------------------------------------\n");
  printf("1st Order Differentiation\n");
  printf("Weights\n");
  print_Vector(C);
  
  sol = 0;
  for(i=0;i<8;i++){
    sol += C[i]*pow((((double)i)/7.0),7.0);
  }
  printf("x=0.5   numerical value   analytical value    absolute error\n");
  printf("0.5     %13.10f      %13.10f      %13.10f\n",sol,7.0*pow(0.5,6.0),fabs(sol - 7.0*pow(0.5,6.0)));

  double weight_sec_dif[8] = {
  0.0,
  0.0,
  2.0,
  6.0*0.5,
  12.0*pow(0.5,2.0),
  20.0*pow(0.5,3.0),
  30.0*pow(0.5,4.0),
  42.0*pow(0.5,5.0)
  };


  dgemv(lN,&n,&n,&alpha,B_I,&n,weight_sec_dif,&one,&beta,C,&one);
  printf("\n---------------------------------------------------------------\n");
  printf("2nd Order Differentation\n");
  printf("Weights\n");
  print_Vector(C);

  sol = 0;
  for(i=0;i<8;i++){
    sol += C[i]*pow((((double)i)/7.0),7.0);
  }
  printf("x=0.5   numerical value   analytical value    absolute error\n");
  printf("0.5     %13.10f      %13.10f      %13.10f\n",sol,42.0*pow(0.5,5.0),fabs(sol - 42.0*pow(0.5,5.0)));


  double weight_int[8] = {
    0.5,
    0.5*pow(0.5,2.0),
    (1.0/3.0)*pow(0.5,3.0),
    (1.0/4.0)*pow(0.5,4.0),
    (1.0/5.0)*pow(0.5,5.0),
    (1.0/6.0)*pow(0.5,6.0),
    (1.0/7.0)*pow(0.5,7.0),
    (1.0/8.0)*pow(0.5,8.0)
  };


  dgemv(lN,&n,&n,&alpha,B_I,&n,weight_int,&one,&beta,C,&one);
  printf("\n---------------------------------------------------------------\n");
  printf("Integration\n");
  printf("Weights\n");
  print_Vector(C);

  sol = 0;
  for(i=0;i<8;i++){
    sol += C[i]*pow((((double)i)/7.0),7.0);
  }
  printf("x=0.5   numerical value   analytical value    absolute error\n");
  printf("0.5     %13.10f      %13.10f      %13.10f\n",sol,(pow(0.5,8.0)/8.0),fabs(sol - (pow(0.5,8.0)/8.0)));


  return 0;
}

void print_Matrix(double array[64]){
  int i,j;
  for(i=0;i<8;i++){
    for(j=0;j<8;j++){
      printf("%16.6f",array[i+8*j]);
    }
   printf("\n");
  }
  printf("\n");
}

void print_Vector(double array[8]){
  int i;
  for(i=0;i<8;i++){
    printf("%16.6f \n",array[i]);
  }
  printf("\n");
}
