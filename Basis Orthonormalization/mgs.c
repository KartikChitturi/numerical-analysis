#include<stdio.h>
#include<math.h>
#include<stdlib.h>

void print_normalized(double vec[3]);
double dot(double vec1[3], double vec2[3]);

int main(){
  //The method takes in rows so the matrix A has been transposed
  double A[3][3] = {{1.6543,2.5643,2.6432},{-1.5421,0.54231,2.5431},{0.5652,0.1545,1.8754}};
  double B[3];
  double z[3];
  int i = 0;
  int j = 0;
  int k = 0;
  double dotBz, dotzz;

  printf("Using Modified Gram-Schmidt, the normalized orthogonal vectors are:\n");
  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      B[j] = A[i][j];
    }    
    for(j=0;j<i;j++){
      for(k=0;k<3;k++){
        z[k] = A[j][k];
      }
      dotBz = dot(B,z);
      dotzz = dot(z,z);
 
      for(k=0;k<3;k++){
        B[k] -= (dotBz/dotzz)*z[k];
      }
    }
    for(j=0;j<3;j++){
      A[i][j] = B[j];
    }   
  }


  for(i=0;i<3;i++){
    for(j=0;j<3;j++){
      B[j] = A[i][j];
    }
    print_normalized(B);
  }

  return 0;
}

void print_normalized(double vec[3]){
  int i = 0;
  double mag;
  mag = sqrt(dot(vec,vec));
  if(mag==0.0){
    printf("[%25.22f, %25.22f, %25.22f]\n",0.0,0.0,0.0);
  }
  else{
    printf("[%25.22f, %25.22f, %25.22f]\n",vec[0]/mag,vec[1]/mag,vec[2]/mag);
  }
}

double dot(double vec1[3], double vec2[3]){
  double sol = 0.0;
  int i = 0;
  for(i=0;i<3;i++){
    sol += vec1[i]*vec2[i];
  }
  return sol;
}
