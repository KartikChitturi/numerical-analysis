#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){
  double sum = 0.0;
  int i = 0;
  int j =0;
  double x;
  int limit;
  for(i=1;i<6;i++){
    sum = 0.0;
    limit = (int) pow(10.0,(double) i);
    for(j=0;j<limit;j++){
      x = (double)rand()/RAND_MAX;
      x = -5.0+10.0*x;
      sum += exp(-x*x/2)*(5.0+cos(x));
    }
    printf("%-6d %f\n",limit,sum*(10.0/((double)limit)));
  }
  //For the uniform distribution we divide by (1/10) for the probability distribution (unbiased fator)
  //And divide again by the number of points. This is the same as multiplying by (10/# of points)
  

  return 0;
}
