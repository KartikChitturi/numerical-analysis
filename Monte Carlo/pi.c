#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){
  int i,j = 0;
  double r;
  double x,y;
  int red;
  int limit;
  for(i=1;i<6;i++){
    red = 0;
    limit = (int) pow(10.0,(double) i);
    for(j=0;j<limit;j++){
      x = (double)rand()/RAND_MAX;
      y = (double)rand()/RAND_MAX;
      r = sqrt(pow(x,2.0)+pow(y,2.0));
      if (r <= 1){
        red +=1;
      }
    }
    printf("%-6d %f\n",limit,4*red/((double)limit));
  }
  return 0;
}
