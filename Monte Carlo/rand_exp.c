#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){

  FILE * f;
  f=fopen("rand_exp.dat","w");

  int i=0;
  double x;
  for(i=0;i<100000;i++){
    x = (double)rand()/RAND_MAX;
    fprintf(f,"%f\n",-3*log(1-x));
  }

  fclose(f);
  return 0;
}
