#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){

  int limit = 0;
  double u;
  double y;
  int num_x = 0;

  FILE * f;
  f =fopen("rand_normal.dat","w");

  while(limit == 0){
    u = (double)rand()/RAND_MAX;
    y = (double)rand()/RAND_MAX;
    y = -5.0 + 10.0*y;

    if(u <= exp(-y*y/2.0)){
      num_x = num_x + 1;
      fprintf(f,"%f\n",y);
     }
    if(num_x == 100000){
      limit = 1; 
    }
  }
  fclose(f);

  return 0;
}
