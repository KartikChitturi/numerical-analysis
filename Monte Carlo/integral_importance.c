#include<stdio.h>
#include<stdlib.h>
#include<math.h>

int main(){

  double sum = 0.0;
  int i = 0;
  int j =0;
  double x,u,y;
  int k=0;
  int limit;
  double pi = 3.14159265359;
  for(i=1;i<6;i++){
    sum = 0.0;
    limit = (int) pow(10.0,(double) i);
    for(j=0;j<limit;j++){
      k+=1;

      u = 2.0;
      y = 0.0;
      while(u > exp(-y*y/2.0)){
        u = (double)rand()/RAND_MAX;
        y = (double)rand()/RAND_MAX;
        y = -5.0 + 10.0*y;
      }


      sum += 1*(5.0+cos(y));
    }
    printf("%-6d %f\n",limit,sqrt(2*pi)*sum/((double)limit));
  }

  return 0;
}



